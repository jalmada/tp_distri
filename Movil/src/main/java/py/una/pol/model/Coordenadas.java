package py.una.pol.model;

import java.io.Serializable;
import java.util.Date;



public class Coordenadas implements Serializable {

	private Movil movil;
	private Date fecha;
	private Long latitud;
	private Long longitud;


	public Movil getMovil() {
		return movil;
	}

	public void setMovil(Movil movil) {
		this.movil = movil;
	}


	public Date getFecha() {
		return fecha;
	}


	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}


	public Long getLatitud() {
		return latitud;
	}


	public void setLatitud(Long latitud) {
		this.latitud = latitud;
	}


	public Long getLongitud() {
		return longitud;
	}


	public void setLongitud(Long longitud) {
		this.longitud = longitud;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Ubicacion [latitud=");
		builder.append(latitud);
		builder.append(", longitud=");
		builder.append(longitud);
		builder.append(", movil=");
		builder.append(movil);
		builder.append("]");
		return builder.toString();
	}

}
