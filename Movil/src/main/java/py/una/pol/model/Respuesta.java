package py.una.pol.model;

import java.io.Serializable;
import java.util.Date;


public class Respuesta implements Serializable {
	
	private Movil movil;
	private Long distancia;
	private Date fecha;

	public Movil getMovil() {
		return movil;
	}




	public void setMovil(Movil movil) {
		this.movil = movil;
	}




	public Long getDistancia() {
		return distancia;
	}




	public void setDistancia(Long distancia) {
		this.distancia = distancia;
	}




	public Date getFecha() {
		return fecha;
	}




	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}




	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DistanciaResponse [distancia=");
		builder.append(distancia);
		builder.append(", movil=");
		builder.append(movil);
		builder.append(", fecha=");
		builder.append(fecha);
		builder.append("]");
		return builder.toString();
	}

}
