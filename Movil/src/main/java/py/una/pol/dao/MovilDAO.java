package py.una.pol.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.model.Coordenadas;
import py.una.pol.model.Movil;


@Stateless
public class MovilDAO {

	@Inject
	private Logger log;

	public long registrarMovil(Movil movil) throws SQLException {
		String sql = "INSERT INTO movil(id_movil, tipo) VALUES(?,?)";

		long id = 0;
		Connection conn = null;

		try {
			conn = Bd.connect();
			PreparedStatement pstmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			pstmt.setLong(1, movil.getId());
			pstmt.setString(2, movil.getTipo());
			int affectedRows = pstmt.executeUpdate();
			if (affectedRows > 0) {
				// get the ID back
				try (ResultSet rs = pstmt.getGeneratedKeys()) {
					if (rs.next()) {
						id = rs.getLong(1);
					}
				} catch (SQLException ex) {
					throw ex;
				}
			}
		} catch (SQLException ex) {
			throw ex;
		} finally {
			try {
				conn.close();
			} catch (Exception ef) {
				log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
			}
		}
		log.info("Movil registrado");
		return id;
	}
	
	public long registrarCoord(Coordenadas coordenada) throws SQLException {
		String sql = "INSERT INTO coordenada(latitud, longitud, id_movil, fechahora) VALUES(?,?,?,?)";
		long id = 0;
		Connection conn = null;
		if(coordenada.getLatitud()!=null || coordenada.getLongitud()!=null) {
			try {
			conn = Bd.connect();
			PreparedStatement pstmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			pstmt.setLong(1, coordenada.getLatitud());
			pstmt.setLong(2, coordenada.getLongitud());
			pstmt.setLong(3, coordenada.getMovil().getId());
			pstmt.setDate(4, new java.sql.Date(System.currentTimeMillis()));
			
			int affectedRows = pstmt.executeUpdate();
			if (affectedRows > 0) {
				try (ResultSet rs = pstmt.getGeneratedKeys()) {
					if (rs.next()) {
						id = rs.getLong(1);
					}
				} catch (SQLException ex) {
					throw ex;
				}
			}
		} catch (SQLException ex) {
			throw ex;
		} finally {
			try {
				conn.close();
			} catch (Exception ef) {
				log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
			}
		}
		}
		log.info("Ubicación registrada");
		return id;
	}

	
	public static Date parseFecha(String fecha) {
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date fechaDate = null;
		try {
			fechaDate = formato.parse(fecha);
		} catch (ParseException ex) {
			System.out.println(ex);
		}
		return fechaDate;
	}
	
	public List<Coordenadas> listar(Long latitud, Long longitud) {
		String query = "SELECT m.id, m.tipo, co.latitud, co.longitud, co.fechahora FROM coordenada co INNER JOIN movil m ON co.id_movil = m.id_movil ORDER BY fechahora DESC";
		List<Coordenadas> lista = new ArrayList<>();
		Connection conn = null;
		try {
			conn = Bd.connect();
			ResultSet rs = conn.createStatement().executeQuery(query);
			while (rs.next()){
				Coordenadas coord = new Coordenadas();
				Movil movil = new Movil();
				movil.setId(rs.getLong(1));
				movil.setTipo(rs.getString(2));
				coord.setMovil(movil);
				coord.setLatitud(rs.getLong(3));
				coord.setLongitud(rs.getLong(4));
				String fecha = rs.getString(5);
				coord.setFecha(parseFecha(fecha));
				lista.add(coord);
			}
		} catch (SQLException ex) {
			log.severe("Error en la seleccion: " + ex.getMessage());
		} finally {
			try {
				conn.close();
			} catch (Exception ef) {
				log.severe("No se pudo cerrar la conexion a BD: " + ef.getMessage());
			}
		}
		return lista;
	}
}


	