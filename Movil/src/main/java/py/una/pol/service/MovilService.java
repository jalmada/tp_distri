/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.una.pol.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;

import py.una.pol.dao.MovilDAO;
import py.una.pol.model.Coordenadas;
import py.una.pol.model.Movil;
import py.una.pol.model.Respuesta;

public class MovilService {

	@Inject
	private Logger log;

	@Inject
	private MovilDAO dao;

	public void registrar(Movil movil) throws Exception {
		try {
			dao.registrarMovil(movil);
		} catch (Exception e) {
			log.severe("Error al registrar el movil");
			throw e;
		}
	}

	public void registrarCoord(Coordenadas c) throws Exception {
		try {
			dao.registrarCoord(c);
		} catch (Exception e) {
			log.severe("Error al registrar la ubicacion");
			throw e;

		}
	}

	public List<Respuesta> listar(Long latitud, Long longitud) {
		List<Respuesta> moviles = new ArrayList<>();
		List<Coordenadas> ubicaciones = dao.listar(latitud, longitud);

		for (Coordenadas ubicacion : ubicaciones) {
			Long longitud1 = longitud;
			Long latitud1 = latitud;
			Long longitud2 = ubicacion.getLongitud();
			Long latitud2 = ubicacion.getLatitud();
			Long distancia = (long) Math.sqrt(
					(longitud2 - longitud1) * (longitud2 - longitud1) + (latitud2 - latitud1) * (latitud2 - latitud1));
			if (distancia < 20) {
				Respuesta response = new Respuesta();
				response.setMovil(ubicacion.getMovil());
				response.setDistancia(distancia);
				response.setFecha(ubicacion.getFecha());
				moviles.add(response);
			}
		}
		return moviles;
	}
}
