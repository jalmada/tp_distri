package py.una.pol.rest;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import py.una.pol.model.Coordenadas;
import py.una.pol.model.Movil;
import py.una.pol.model.Respuesta;
import py.una.pol.service.MovilService;

@Path("/movil")
@RequestScoped
public class MovilRESTService {


	@Inject
	MovilService movilService;
	
	@GET
	@Path("/ubicacion")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Respuesta> ubicaciones(@QueryParam("latitud") Long latitud,
			@QueryParam("longitud") Long longitud) {
		return movilService.listar(latitud, longitud);
	}
	
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response registrar(Movil movil) {
		Response.ResponseBuilder builder = null;
		try {
			movilService.registrar(movil);
			builder = Response.status(Response.Status.CREATED).entity("¡Movil Registrado!");
		} catch (SQLException e) {
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("bd-error", e.getLocalizedMessage());
			builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
		} catch (Exception e) {
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("error", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
		}
		return builder.build();
	}
	
	@POST
	@Path("/ubicacion")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response guardarUbicacion(Coordenadas c) {
		Response.ResponseBuilder builder = null;
		try {
			movilService.registrarCoord(c);
			builder = Response.status(Response.Status.CREATED).entity("¡Ubicacion registrada!");
		} catch (SQLException e) {
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("bd-error", e.getLocalizedMessage());
			builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
		} catch (Exception e) {
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("error", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
		}
		return builder.build();
	}

	
}
