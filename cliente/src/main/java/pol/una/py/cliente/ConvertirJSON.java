package pol.una.py.cliente;

import org.json.simple.JSONObject;

public class ConvertirJSON {
	@SuppressWarnings("unchecked")
	
	public static String generarUbicacionMovilJSON(long latitud, long longitud, long id, String tipo) {
		JSONObject obj = new JSONObject();
		obj.put("latitud", latitud);
		obj.put("longitud", longitud);
		JSONObject objMovil = new JSONObject();
		objMovil.put("id", id);
		objMovil.put("tipo", tipo);
		obj.put("movil", objMovil);
		return obj.toJSONString();
	}
	
	public static String generarMovilJSON(long id, String tipo) {
		JSONObject obj = new JSONObject();
		obj.put("id", id);
		obj.put("tipo", tipo);
		return obj.toJSONString();
	}
}