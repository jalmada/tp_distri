package py.una.pol.model;

public class Persona {
	private Long cedula;
	private String nombre;
	private String apellido;
	private String numero;
	private String contrasenha;

	public Persona() {

	}

	public Persona(Long cedula, String nombre, String apellido, String numero, String contrasenha) {
		this.cedula = cedula;
		this.nombre = nombre;
		this.apellido = apellido;
		this.numero = numero;
		this.contrasenha = contrasenha;
	}

	public Long getCedula() {
		return cedula;
	}

	public void setCedula(Long cedula) {
		this.cedula = cedula;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getContrasenha() {
		return contrasenha;
	}

	public void setContrasenha(String contrasenha) {
		this.contrasenha = contrasenha;
	}

}
