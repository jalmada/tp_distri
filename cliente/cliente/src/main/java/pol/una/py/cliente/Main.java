package pol.una.py.cliente;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;

public class Main {

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		String seleccion = "";
		
		System.out.println("Bienvenido al Trabajo de Sistemas Distribuidos");
		do {
			System.out.println("\tPor favor seleccione una opcion");
			System.out.println("");
			System.out.println("A) Registrar Movil /n B) Ver distancia entre los moviles /n C) Registrar coordenadas /n D)- Salir");
			seleccion = entrada.next();
			seleccion = seleccion.toLowerCase();
			if(seleccion.equals("a")) {
				long id;
				String tipo;
				
				System.out.println("Ingrese id del movil");
				id = entrada.nextLong();
				System.out.println("Ingrese tipo de movil");
				tipo = entrada.next();
				registrar(id, tipo);
			}
			if(seleccion.equals("b")) {
				System.out.println("Ingrese latitud");
				long latitud4 = entrada.nextLong();
				System.out.println("Ingrese longitud");
				long longitud4 = entrada.nextLong();
				distancias(latitud4, longitud4);
			}
			if(seleccion.equals("c")) {
				System.out.println("Ingrese latitud");
				long latitud = entrada.nextLong();
				System.out.println("Ingrese longitud");
				long longitud = entrada.nextLong();
				System.out.println("Ingrese id movil");
				long ide = entrada.nextLong();
				System.out.println("Ingrese tipo movil");
				String tipoM = entrada.next();
				registrarCoordenadas(latitud, longitud, ide, tipoM);
			}

		} while (seleccion.equals("d") );
	}

	public static void registrar(long id, String tipo) {
		try { 

			URL URL = new URL("http://localhost:8080/Movil/rest/movil");
			HttpURLConnection connection = (HttpURLConnection) URL.openConnection();
			connection.setDoOutput(true);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			

			String input = Convertidor.generarMovilJSON(id, tipo);
			System.out.println(input);
			OutputStream os = connection.getOutputStream();
			os.write(input.getBytes());
			os.flush();

			if (connection.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
				throw new RuntimeException("ERROR : " + connection.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((connection.getInputStream())));

			String output;
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}
			connection.disconnect();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();

		}

	}

	
	public static void registrarCoordenadas(long la, long lo, long id, String tipo) {
		try {

			URL URL = new URL("http://localhost:8080/Movil/rest/movil/ubicacion");
			HttpURLConnection connection = (HttpURLConnection) URL.openConnection();
			connection.setDoOutput(true);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			

			String input = Convertidor.generarUbicacionMovilJSON(la, lo, id, tipo);
			OutputStream os = connection.getOutputStream();
			os.write(input.getBytes());
			os.flush();

			if (connection.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
				throw new RuntimeException("ERROR : " + connection.getResponseCode());
			}
			BufferedReader br = new BufferedReader(new InputStreamReader((connection.getInputStream())));

			String output;
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}
			connection.disconnect();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void distancias(long latitud, long longitud) {
		try {
			String query = "http://localhost:8080/Movil/rest/movil" + "/ubicacion?latitud=" + latitud + "&longitud=" + longitud;
			System.out.println("Conectando " + query);
			URL URL = new URL(query);
			HttpURLConnection conn = (HttpURLConnection) URL.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Content-Type", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("ERROR : " + conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			JSONObject rps = new JSONObject();
			JSONArray json = new JSONArray(br.readLine());
			System.out.println("Recibimos : " + json);
			
			conn.disconnect();

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}