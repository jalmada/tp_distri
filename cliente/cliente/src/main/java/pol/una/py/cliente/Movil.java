package pol.una.py.cliente;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Movil implements Serializable {
	private String tipo;
	private Long id;

	public Movil(Long id, String tipo) {
		this.id = id;
		this.tipo = tipo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public Long getid() {
		return id;
	}

	public void setid(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Movil [id=");
		builder.append(id);
		builder.append(", tipo=");
		builder.append(tipo);
		builder.append("]");
		return builder.toString();
	}

}
